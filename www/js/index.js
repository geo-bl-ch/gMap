//Externe Library für die "Swipe - Bewegungen"
var mySwiper = new Swiper ('.swiper-container', {
    paginationClickable: true,
    spaceBetween: 30,
      // If we need pagination
    pagination: '.swiper-pagination',
    loop: true,
    direction: 'horizontal'
}); 
