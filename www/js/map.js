//window.location.search beinhaltet die "GET"-Parameter (url)
var mapName = window.location.search;

//Auswahlverfahren anhand von "mapName"
if (mapName.includes('world') == true) {
     var map = L.map('map', {
          maxBounds: [[-85.1114157806266, -181.40625], [85.0511287798066, 180.70312500000003]]
     }).setView([46.50784482789971, 7.643737792968751], 6);
     var tileLayer = 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png';
     var layers = '';
     var href = 'http://openstreetmap.org';
     var attribution = 'OpenStreetMap';
     var minZoom = 1;
}
else if (mapName.includes('bl') == true) {
     var map = L.map('map', {
          crs: L.CRS.EPSG3857
          , maxBounds: [[45.817315080406274, 5.943603515625001], [47.8094654494779, 10.5084228515625022]]
     }).setView([47.477478022384005, 7.7523136138916025], 13);
     var tileLayer = 'http://geowms.bl.ch';
     var layers = 'grundkarte_farbig_group';
     var href = 'https://www.baselland.ch/politik-und-behorden/direktionen/volkswirtschafts-und-gesundheitsdirektion/amt-fur-geoinformation';
     var attribution = 'GIS Basel-Landschaft';
     var minZoom = 8;
}
else if (mapName.includes('orthofoto') == true) {
     var map = L.map('map', {
          crs: L.CRS.EPSG3857
          , maxBounds: [[47.33789206010505, 7.320327758789063], [47.634395502617345, 7.994613647460938]]
     }).setView([47.48983296422693, 7.735919952392579], 13);
     var tileLayer = 'http://geowms.bl.ch';
     var layers = 'orthofotos_agi_2015_group';
     var href = 'https://www.baselland.ch/politik-und-behorden/direktionen/volkswirtschafts-und-gesundheitsdirektion/amt-fur-geoinformation';
     var attribution = 'GIS Basel-Landschaft';
     var minZoom = 11;
}

//Einstellungen für die geladene Karte
L.tileLayer.wms(tileLayer, {
     layers: layers
     , attribution: 'Map data &copy; <a href="' + href + '">' + attribution + '</a>'
     , maxZoom: 18
     , minZoom: minZoom
     , noWrap: true //Boolean "noWrap": Wiederholung der Karte deaktiviert
}).addTo(map);

//Variablen
var watchID = null;
var watchID2 = null;
var initialized = false;
var debug = true;
var lastX = 0;
var lastY = 0;
var lastZ = 0;
var accX = 0;
var axxY = 0;
var accZ = 0;
var latitude = L.latLng(map.getCenter()).lat;
var longitude = L.latLng(map.getCenter()).lng;

//Benötigt für Start/Stopp Sensor
var zahl = 0;

// benötigt für z messung
var pause = 0;
var pauseZ = 0;
var deltaZLast = 0;
var zPlay = true;
var counter = 0;
var counterZ = 0;
var resetCounter = 0;

// prüfe, ob hybrid-app oder Browser
var browser = false;

//Accelerometer Sensor kann nur so gestartet werden
var app = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

if (app) { // phonegap application
     document.addEventListener("deviceready", onDeviceReady, false);
}
else { // web     
     browser = true;
     window.addEventListener("devicemotion", devicemotionHandler, true);
}

// Funktion behandelt accelerometer events für Browser
function accelerometerSuccess(acceleration) {
     
     //Aktuelle Beschleunigung
     accX = acceleration.x; // nach links, rechts
     accY = acceleration.y; // nach oben, unten
     accZ = acceleration.z; // nach vorne, hinten
     
     //Zurücksetzen des Sensors
     if (!initialized) {
          lastX = accX;
          lastY = accY;
          lastZ = accZ;
          initialized = true;
          counterZ = 0;
     }
     //document.getElementById('x').innerHTML = 'X: ' + acceleration.x;
     //document.getElementById('y').innerHTML = 'Y: ' + acceleration.y;
     //document.getElementById('z').innerHTML = 'Z: ' + acceleration.z;
     
     //überwachen der Bewegung in X und Y
     var deltaX = minimalValueX(lastX - accX);
     var deltaY = minimalValueY(lastY - accY);
     var deltaZ = 0;
     if (pause > 0) { // nach jedem Zoom 
          pause -= 1;
     }
     else {
          deltaZ = minimalZoom(lastZ - accZ);
     }
     //document.getElementById('dZ').innerHTML = 'dZ: ' + deltaZ;
     
     //Warnung, wenn Device längere Zeit zu steil gehalten wird
     if (accY >= 9 || accY <= -9 || accX >= 6 || accX <= -6) {
          counterZ += 1;
          if (counterZ > 5) {
               deltaZ = 0;
               bootstrap_alert('#alertSensor', 'dieser Text wird überschrieben....');
               pauseZ = 50;
          }
     }
     else {
          counterZ = 0;
          $('#alertSensor').hide();
     }
     if (pauseZ > 0) { // pause, damit Map nicht wegdriftet nach Warnung
          pauseZ -= 1;
          initialized = false;
     }
     
     // aktuelles Zoom, kleinstes Zoom
     var zoom = getZoom();

     //Schnelligkeit der Bewegung angepasst auf den Zoom
     getLonLatShiftByZoom(zoom, deltaX, deltaY);
     
     //anwenden der berechneten Verschiebung resp. Zoom 
     if (deltaX == 0 && deltaY == 0 && deltaZ != 0) {
          stufe = map.getZoom() - deltaZ;
          map.setZoom(stufe);
          pause = 20;
     }
     else {
          map.setView(L.latLng(latitude, longitude));
     }
}

// Funktion behandelt accelerometer events für Apps
function onSuccess(acceleration) {
     //Aktuelle Beschleunigung
     accX = acceleration.x; // nach links, rechts
     accY = acceleration.y; // nach oben, unten
     accZ = acceleration.z; // nach vorne, hinten

     //Zurücksetzen des Sensors
     if (!initialized) {
          lastX = accX;
          lastY = accY;
          lastZ = accZ;
          initialized = true;
          counter = 0;
          deltaZLast = 0;
          counterZ = 0;
     }
     
     //überwachen der Bewegung in X und Y
     var deltaX = minimalValueX(lastX - accX);
     var deltaY = minimalValueY(lastY - accY);
     
     //überwachen der Bewegung in Z Richtung
     var deltaZ = 0; //minimalZoom(lastZ - accZ);	    
     if (pause > 0) { // nach jedem Zoom 
          pause -= 1;
          lastZ = accZ;
          counter = 0;
          counterZ = 0;
          deltaZLast = 0;
     }
     else {
          deltaZ = minimalZoom(lastZ - accZ);
          if (deltaZ != 0) {
               writeLog("accZ: " + accZ + ", deltaZ: " + deltaZ + ", counter: " + counter);
               if (counter == 0) {
                    deltaZLast = deltaZ; // merke dir beim 0. Durchgang das deltaZ
                    deltaZ = 0; // und setze deltaZ nochmals auf 0, also kein Zoom
                    restartWatch();
               }
               else if (counter == 1) {
                    if (deltaZ == deltaZLast) {
                         deltaZ = 0; // verwerfe die 1. Messung
                         restartWatch(); // damit sicher erneut gemessen wird                  
                    }
               }
               counter += 1;
          }
          else {

               // reset counter nach 1 sec falls eine Messreihe fehlschlägt
               if (resetCounter > 100) {
                    counter = 0;
                    resetCounter = 0;
                    counterZ = 0;
                    deltaZ = 0;
                    //deltaZLast = 0;
               }
               else {
                    resetCounter += 1;
               }
          }
     }
 
     //Warnung, wenn Device längere Zeit zu steil gehalten wird
     if (accY >= 9 || accY <= -9 || accX >= 6 || accX <= -6) {
          counterZ += 1;
          if (counterZ > 5) {
               deltaZ = 0;
               //deltaZLast = 0;
               bootstrap_alert('#alertSensor', 'dieser Text wird überschrieben....');
               pauseZ = 50;
          }
     }
     else {
          counterZ = 0;
          $('#alertSensor').hide();
     }
     if (pauseZ > 0) { // pause, damit Map nicht wegdriftet nach Warnung
          pauseZ -= 1;
          initialized = false;
     }
     
     // aktuelles Zoom, kleinstes Zoom
     var zoom = getZoom();
     
     //Schnelligkeit der Bewegung angepasst auf den Zoom
     getLonLatShiftByZoom(zoom, deltaX, deltaY);
     
     //anwenden der berechneten Verschiebung resp. Zoom 
     if (deltaX == 0 && deltaY == 0 && deltaZ != 0) {
          if (deltaZLast != deltaZ && counter >= 2) {
               writeLog("deltaZ approved");
               stufe = map.getZoom() + deltaZ;
               map.setZoom(stufe);
               if (watchID) {
                    watchID = navigator.accelerometer.clearWatch(watchID);
                    watchID = null;
               }
               pause = 20; // 1/5 sec 
          }
          else {
               writeLog("deltaZ ambiguous");
               //deltaZLast = 0;
               pause = 10;
          }
     }
     else {
          map.setView(L.latLng(latitude, longitude));
     }
}

// bestimme Zoom
function getZoom(){
var zoom = map.getZoom();
     if (map.getZoom() == 0 || map.getZoom() == 1) {
          zoom = map.getZoom() + 1;
     }
     else {
          zoom = map.getZoom();
     }
     return zoom;
}

// bestimme Schnelligkeit beim Map verschieben
function getLonLatShiftByZoom(zoom, deltaX, deltaY){
     switch (zoom) {
     case 0:
     case 5:
     case 3:
     case 4:
     case 2:
     case 6:
          longitude = longitude + (deltaX / (zoom * zoom));
          latitude = latitude + (deltaY / (zoom * zoom));
          break;
     case 1:
          longitude = longitude + (deltaX / (zoom));
          latitude = latitude + (deltaY / (zoom));
          break;
     case 7:
          longitude = longitude + (deltaX / (zoom * zoom * 2));
          latitude = latitude + (deltaY / (zoom * zoom * 2));
          break;
     case 8:
     case 9:
          longitude = longitude + (deltaX / (zoom * zoom * 3));
          latitude = latitude + (deltaY / (zoom * zoom * 3));
          break;
     case 10:
     case 11:
          longitude = longitude + (deltaX / (zoom * zoom * 4));
          latitude = latitude + (deltaY / (zoom * zoom * 4));
          break;
     case 12:
          longitude = longitude + (deltaX / (zoom * zoom * 10));
          latitude = latitude + (deltaY / (zoom * zoom * 10));
          break;
     case 13:
          longitude = longitude + (deltaX / (zoom * zoom * zoom));
          latitude = latitude + (deltaY / (zoom * zoom * zoom));
          break;
     case 14:
     case 15:
          longitude = longitude + (deltaX / (zoom * zoom * zoom * 3));
          latitude = latitude + (deltaY / (zoom * zoom * zoom * 3));
          break;
     default:
          longitude = longitude + (deltaX / (zoom * zoom * zoom * zoom));
          latitude = latitude + (deltaY / (zoom * zoom * zoom * zoom));
          break;
     }     
}

//Fehlermeldung - Accelerometer
function onError() {
     alert('Ein Fehler ist aufgetreten. Versuchen Sie es später nochmal.')
}

function onDeviceReady() {
     startWatch();
}

function devicemotionHandler(event) {
     accelerometerSuccess(event.accelerationIncludingGravity);
}

//Accelerometer Startfunktion
function startWatch() {
     if (!browser) {
          var options = {
               frequency: 10
          };
          watchID = null;
          watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);
     }
     else {
          window.addEventListener("devicemotion", devicemotionHandler, true);
     }     
}

//Accelerometer Stop
function stopWatch(){
     if (!browser) {
          if (watchID) {
               watchID = navigator.accelerometer.clearWatch(watchID);
               watchID = null;
          }
     }
     else {
          window.removeEventListener("devicemotion", devicemotionHandler, true);
     }     
}

//Acclereometer Restarte
function restartWatch() {
     //writeLog("restart accelerometer");
     if (watchID) {
          watchID = navigator.accelerometer.clearWatch(watchID);
          startWatch();
     }
}

//Toleranz, ab wann der Sensor sich bewegen darf
function minimalValueX(accValue) {
     if (accValue < 2 && accValue > -2) {
          return 0;
     }
     else {
          return accValue;
     }
}

function minimalValueY(accValue) {
     if (accValue < 1.5 && accValue > -1.5) {
          return 0;
     }
     else {
          return accValue;
     }
}

function minimalZoom(zoomValue) {
     if (!zPlay) {
          return 0;
     }
     else if (zoomValue > 1.5) {
          return 1;
     }
     else if (zoomValue < -1.5) {
          return -1;
     }
     else {
          return 0;
     }
}

//Accelerometer Reset
map.on('click', function () {
     initialized = false;
}, false);

//Sensorstopp bei Bewegung mit dem Finger
map.on('dragstart', function () {
     stopWatch();
});

//Sensorstart beim Aufhören der Bewegungen mit dem Finger
map.on('dragend', function () {
     initialized = false;
     latitude = L.latLng(map.getCenter()).lat;
     longitude = L.latLng(map.getCenter()).lng;
     if (zahl != 999) {
          startWatch();
     }
});

//Funktion, damit man beim Zoomen nicht immer von der Sensor Funktion in das Center gezogen wird
map.on('zoomend', function (e) {
     writeLog("Zoomstufe: " + map.getZoom());
     // starte Accelerometer neu, falls gestoppt
     if (!browser) {
          if (!watchID) {
               initialized = false;
               latitude = L.latLng(map.getCenter()).lat;
               longitude = L.latLng(map.getCenter()).lng;
               startWatch();
          }
     }
});

//Button zum Starten und Stoppen des Sensors
L.easyButton({
     states: [{
          stateName: 'start'
          , icon: 'fa-play'
          , title: 'Stoppt den Sensor'
          , onClick: function (control) {
               stopWatch();
               zahl = 999;
               control.state('stop');
               window.zButton.state('Z-Stop');
          }
    }, {
          stateName: 'stop'
          , icon: 'fa-stop'
          , title: 'Startet den Sensor'
          , onClick: function (control) {
               startWatch();
               zahl = 1;
               control.state('start');
               window.zButton.state('Z-Start');
          }
    }]
     , position: 'topright'
}).addTo(map);

// var z_stop = 'data:image/png;base64,/../img/z_stop.png';
//var z_stop = '/../img/z_stop.png'; 
//Button zum Starten und Stoppen des Z-Sensors
zButton = L.easyButton({
     states: [{
          stateName: 'Z-Start'
          , icon: '<strong>z</strong>'
          , title: 'Stoppt den Z-Sensor'
          , onClick: function (control) {
               zPlay = false;
               control.state('Z-Stop');
          }
    }, {
          stateName: 'Z-Stop'
          , icon: '<i id="z_blocked" class="material-icons">block</i>'
          , title: 'Startet den Z-Sensor'
          , onClick: function (control) {
               zPlay = true;
               control.state('Z-Start');
          }
    }]
     , position: 'topright'
}).addTo(map);

//Button zur Lokalisierung der Position
L.easyButton({
     states: [{
          stateName: 'unloaded'
          , icon: 'icon ion-android-locate'
          , title: 'Lokalisierung'
          , onClick: function (control, map) {
               control.state('loading');
               map.locate({
                    setView: true
               });
               map.on('locationfound', function (e) {
                    L.popup().setContent('Du befindest dich hier').setLatLng(e.latlng).addTo(map);
                    control.state('loaded');
               });
               control._map.on('locationerror', function () {
                    control.state('error');
               });
          }
    }, {
          stateName: 'loading'
          , icon: 'fa-spinner fa-spin'
          , title: 'Wird lokalisiert...'
    }, {
          stateName: 'error'
          , icon: 'fa-frown-o'
          , title: 'Lokalisierung fehlgeschlagen'
          , onClick: function (control) {
               control.state('unloaded');
          }
    }, {
          stateName: 'loaded'
          , icon: 'fa-thumbs-up'
          , title: 'Lokalisierung erfolgreich'
          , onClick: function (control) {
               control.state('unloaded');
          }
    }]
     , position: 'topright'
}).addTo(map);

//3x Klick bzw. 3x Finger führen zum Index zurück
window.addEventListener('click', function (evt) {
     if (evt.detail === 3) {
          window.history.back();
     }
});

function bootstrap_alert(elem, message, timeout) {
     $(elem).show().html('<div class="alert alert-danger"><div class="row"><div class="col-xs-2"><i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i></div><div class="col-xs-10">In dieser Position ist die Bewegung mittels Sensor nicht möglich.</div></div></div>');
     initialized = false;
};

// Achtung: Logging bremst!
function writeLog(str) {
     if (debug) {
          console.log(str);
     }
};